(function (Drupal, drupalSettings, once) {
  'use strict';

  var formatDate = function (date) {
    return date.getFullYear() + '-' + String(date.getMonth() + 1).padStart(2, "0")
      + '-' + String(date.getDate()).padStart(2, "0");
  };

  var init = function (element, startInput, endInput) {
    var startDate = null;
    var endDate = null;

    if (typeof drupalSettings.date_range_picker !== 'undefined'
      && typeof drupalSettings.date_range_picker[element.id] !== 'undefined') {

      if (typeof drupalSettings.date_range_picker[element.id].start !== 'undefined') {
        startDate = drupalSettings.date_range_picker[element.id].start;
      }

      if (typeof drupalSettings.date_range_picker[element.id].end !== 'undefined') {
        endDate = drupalSettings.date_range_picker[element.id].end;
      }
    }

    // If the user adds more values, all elements are replaced by the ajax call so we have
    // to reinititalize the litepicker values from the input fields.

    if (startDate === null && endDate === null) {
      var startDate = startInput.value;
      var endDate = endInput.value;
    }

    var picker = new Litepicker({
      startInput: startInput,
      endInput: endInput,
      element: element,
      //elementEnd: null,
      firstDay: 1,
      format: "DD.MM.YYYY",
      numberOfMonths: 2,
      numberOfColumns: 2,
      startDate: startDate,
      endDate: endDate,
      zIndex: 9999,
      minDate: null,
      maxDate: null,
      minDays: null,
      maxDays: null,
      selectForward: !1,
      selectBackward: !1,
      splitView: !1,
      inlineMode: !1,
      singleMode: !1,
      autoApply: !1,
      allowRepick: false,
      showWeekNumbers: !1,
      showTooltip: !0,
      hotelMode: !1,
      disableWeekends: !0,
      //scrollToDate: !0,
      mobileFriendly: !1,
      lockDaysFormat: "YYYY-MM-DD",
      lockDays: [],
      bookedDaysFormat: "YYYY-MM-DD",
      bookedDays: [],
      dropdowns: {
        minYear: 1990,
        maxYear: null,
        months: !1,
        years: !1
      },
      buttonText: {
        apply: "Apply",
        cancel: "Cancel",
        previousMonth: '<svg width="11" height="16" xmlns="http://www.w3.org/2000/svg"><path d="M7.919 0l2.748 2.667L5.333 8l5.334 5.333L7.919 16 0 8z" fill-rule="nonzero"/></svg>',
        nextMonth: '<svg width="11" height="16" xmlns="http://www.w3.org/2000/svg"><path d="M2.748 16L0 13.333 5.333 8 0 2.667 2.748 0l7.919 8z" fill-rule="nonzero"/></svg>'
      }, tooltipText: {one: "day", other: "days"}, onShow: function () {
        console.log("onShow callback")
      },
      setup: (picker) => {
        picker.on('selected', function (start, end) {
          this.options.startInput.value = formatDate(start);
          this.options.endInput.value = formatDate(end);
        });
      }
    });
  };

  Drupal.behaviors.daterangepicker = {
    attach: function (context, settings) {
      once('date-range-picker', 'div.field--type-daterange fieldset.form-wrapper', context).forEach(function (widget) {
        let start = widget.querySelector('input.start');
        let end = widget.querySelector('input.end');
        let picker = widget.querySelector('input.litepicker-input');
        init(picker, start, end);
      });
    }
  }
}(Drupal, drupalSettings, once));