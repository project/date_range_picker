CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The [Litepicker](https://wakirin.github.io/Litepicker/) widget allows you to easily select date ranges.


REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

Download [litepicker.js](https://cdn.jsdelivr.net/npm/litepicker/dist/litepicker.js) and drop it into
 libraries/litepicker/litepicker.js in the Drupal root.


CONFIGURATION
-------------

Litepicker supports date ranges but not date & time ranges at the moment. 
When you configure the data range field, make sure its date only, 
otherwise the field widget won't show up.


MAINTAINERS
-----------

 * Siavash Torkzaban (Siav.T) - https://www.drupal.org/user/3500907
 * Jan Stöckler (jan.stoeckler) - https://www.drupal.org/user/103809
 * k4v - https://www.drupal.org/user/744246
 * Christian Schnabl (snable) - https://www.drupal.org/user/845676
